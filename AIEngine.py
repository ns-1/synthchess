from openai import OpenAI
import re 
import configparser

class AIEngine:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')
        apikey = config['openai']['APIKey']
        self.client = OpenAI(
            api_key=apikey
        )

        self.output_pattern = re.compile(r'\[([^\]]*)\]')

    def format_prompt(self, game_pgn, game_fen, legal_moves, color):
        prompt = f"""
                You are a highly advanced chess playing AI in a game with a human player. Given 
                the information below about the state of the game, you must make the best move possible 
                to ensure you will win and beat the player. You must only choose from one of the legal moves
                listed, or else the answer will be wrong. You must think through your answer step by step, explain your reasoning, 
                and determine which one of the legal moves you will play this turn.

                Once you reach a final answer as to what move to play, output the move to play in brackets, such as Final Move: [move].
                Do not output moves in brackets unless it is the final move.

                E.g.

                Final Answer: [e4]

                You are: {color}
                Game Move List: {game_pgn}
                Game Forsyth Edwards Notation: {game_fen}
                Legal Moves: {legal_moves}

                Think step by step through your answer, explain your reasoning, and determine which one of the legal moves you will play this turn. 

                Remember:
                Always output your final answer as one of the legal moves in brackets. 
                White's pieces are capital letters and black's pieces are lowercase letters in the FEN
                """
        
        return prompt

        
    def chess_ai_prompt(self, game_pgn, game_fen, legal_moves, color):
        prompt = self.format_prompt(game_pgn, game_fen, legal_moves, color)
        tries = 0

        while(tries < 3):

            stream = self.client.chat.completions.create(
                model="gpt-3.5-turbo",
                messages=[{"role": "user", "content": prompt}],
                stream=True,
            )

            output = ""

            for chunk in stream:
                if chunk.choices[0].delta.content is not None:
                    print(chunk.choices[0].delta.content, end="")
                    output += chunk.choices[0].delta.content


            match = self.output_pattern.search(output)

            if match:
                return match.group(1)
            
            prompt = prompt + output + "Please output your final answer in brackets, e.g. Final Answer: [move]"

            tries += 1
        
        return None