import chess
import chess.pgn

class ChessGame:
    def __init__(self):
        self.board = chess.Board()
        self.game = chess.pgn.Game()
        self.node = self.game

    def make_move(self, move):
        try:
            move_obj = self.board.push_san(move)
            self.node = self.node.add_variation(move_obj)
        except ValueError as e:
            print(f"Invalid move: {e}")

    def new_game(self):
        self.board = chess.Board()
        self.game = chess.pgn.Game()
        self.node = self.game

    def load_game(self, pgn_file_path):
        with open(pgn_file_path, 'r') as pgn_file:
            self.game = chess.pgn.read_game(pgn_file)
            self.board = self.game.board()
            for move in self.game.mainline_moves():
                self.board.push(move)
            self.node = self.game.end()

    def save_game(self, pgn_file_path):
        with open(pgn_file_path, 'w') as pgn_file:
            exporter = chess.pgn.FileExporter(pgn_file)
            self.game.accept(exporter)

    def print_board(self):
        board_str = str(self.board)
        rows = board_str.split('\n')
        print("  a b c d e f g h")
        for i, row in enumerate(rows):
            print(f"{8 - i} {row}")

    def get_turn(self):
        return "white" if self.board.turn == True else "black"

    def get_game(self):
        return self.game
    
    def get_board(self):
        return self.board
    
    def get_pgn(self):
        exporter = chess.pgn.StringExporter()
        self.game.accept(exporter)
        return str(exporter)