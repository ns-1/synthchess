from ChessGame import ChessGame
from AIEngine import AIEngine

def main():
    game = ChessGame()
    game.new_game()
    ai = AIEngine()

    while not game.get_board().is_game_over():
        game.print_board()

        if game.get_turn() == "white":
            print("Your turn! \n")
            move = input("Enter your move (or 'exit' to quit): ")
            if move.lower() == "exit":
                break
            game.make_move(move)
        else: 
            print("AI Turn! \n")
            output = ai.chess_ai_prompt(game.get_pgn(), game.board.fen(), game.board.legal_moves, game.get_turn())

            print("\nAI Plays move: " + output + "!\n")
            game.make_move(output)
    
    if game.get_board().is_game_over():
        print("Game over!")
        print(game.get_board().result())

if __name__ == "__main__":
    main()